#include "header.h"

/* TreeFree frees all memory associated with a binary 
 * search tree. Must be passed the root of a tree. 
 */

void treeFree(Node root)
{
	if(root==NULL) //if the node does not exist, return
	{
		return;
	}
	if(root->left!=NULL)
	{
		treeFree(root->left); //free the contents of the left subtree
	}
	
	if(root->right!=NULL)
	{
		treeFree(root->right); //free the contents of the right subtree
	}
	free(root->plate); //free the contents of the root 
	free(root->first);
	free(root->last);
	free(root); //free the pointer to the root
	return;
}
