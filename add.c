#include "header.h"

/* Add is a function that adds a node to the given binary search tree.
 * it must be passed the pointer to the root of a binary search tree,
 * the plate you wish to add, and the first and last name of the owner
 * of the plate. This function returns the pointer of the root of the 
 * tree.
 */

Node add(Node root,char* plate,char* first,char* last)
{
	if(root==NULL) //if the node does not exist, add the node here
	{
		root=malloc(sizeof(struct node));
		root->plate=malloc(strlen(plate)+1);		
		root->first=malloc(strlen(first)+1);		
		root->last=malloc(strlen(last)+1);		
		strcpy(root->plate,plate);
		strcpy(root->first,first);
		strcpy(root->last,last);
		root->left=NULL;
		root->right=NULL;
		return root;
	}

	if(strcmp(plate,root->plate)<0) //adds the plate to the left subtree of the current node
	{
		Node temp=add(root->left,plate,first,last);
		root->left=temp;
		return root;
	}
	else //adds the plate to the right subtree of the current node
	{
		Node temp=add(root->right,plate,first,last);
		root->right=temp;
		return root;
	}
}


