#include "header.h"

/* Search searches a binary search tree for a license plate.
 * If the plate is found, the owner's information is 
 * uploaded to the variables first and last to be used in
 * the main program. Must be passed the root of a tree,
 * the plate you want to search for, and pointers for the 
 * first and last name to be stored. Returns a 1 if the node
 * was found, and a 0 if it was not.
 */

int search(Node root,char* plate,char* first,char* last)
{
	if(root==NULL) //if the node does not exist, the plate was not found
	{
		return 0;
	}
	
	int num=strcmp(plate,root->plate); //compare the plate to the current node's plate 

	if(num==0) //if they are the same, return 1
	{
		strcpy(first,root->first);
		strcpy(last,root->last);
		strcpy(plate,root->plate);

		return 1;
	} 
	else if(num<0) //try searching the left subtree
	{
		num=search(root->left,plate,first,last);
		return num;
	}
	else //try searching the right subtree
	{
		num=search(root->right,plate,first,last);
		return num;
	}
}
