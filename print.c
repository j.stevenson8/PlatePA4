#include "header.h"

/* LNR prints the contents of the binary search tree in
 * increasing order. Must be passed the root of a tree.
 */

void LNR(Node root)
{
	if(root==NULL)
	{
		return;
	}

	LNR(root->left);
	printf("Plate: %s Name: %s, %s\n",root->plate,root->last,root->first);
	LNR(root->right);
	return;
}

/* NLR prints the contents of the binary search tree 
 * by printing the root first, the contents of 
 * the left subtree next, and the contents of the right
 * subtree last. Must be passed the root of a tree.
 */

void NLR(Node root)
{
	if(root==NULL)
	{
		return;
	}

	printf("Plate: %s Name: %s, %s\n",root->plate,root->last,root->first);
	LNR(root->left);
	LNR(root->right);
	return;
}

/* LRN prints the contents of a binary search tree by 
 * printing the left subtree, then the right subtree, 
 * then the root. Must be passed the root of a tree.
 */

void LRN(Node root)
{
	if(root==NULL)
	{
		return;
	}

	LNR(root->left);
	LNR(root->right);
	printf("Plate: %s Name: %s, %s\n",root->plate,root->last,root->first);
	return;
}
