#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node{
	char* plate;
	char* first;
	char* last;
	struct node* left;
	struct node* right;
};

typedef struct node* Node;

void LNR(Node);
void NLR(Node);
void LRN(Node);
void treeFree(Node);
Node straverse(Node root);
Node traverse(Node root);
Node delete(Node root,char *plate);
Node add(Node root,char* plate,char* first,char* last);
int search(Node root,char* plate,char* first,char* last);
void freeNode(Node root);
int height(Node root);
int balance(Node root);

