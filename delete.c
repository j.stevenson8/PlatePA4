#include "header.h"

/* Delete deletes a node from the search tree.
 * It does not search the tree to make sure it 
 * is in the tree. Must be passed the root of the 
 * tree and the plate you wish to delete. Returns 
 * the pointer to the root of the tree.
 */

Node delete(Node root,char *plate)
{
	if(strcmp(plate,root->plate)==0) //if the current node is the one to be deleted, delete it
	{
		if(root->left==NULL) //if there is nothing in the left subtree, delete the root and replace it with the right subtree
		{
			Node temp=root->right;
			freeNode(root);
			return(temp);
		}
		//at this point, we know the left subtree is not empty
		if(root->left->right==NULL)
		{
			root->left->right=root->right;
			Node temp=root->left;
			freeNode(root);
			return temp;
		}
		Node prev=root->left;
		Node curr=prev->right;
		while(curr->right!=NULL)
		{
			prev=curr;
			curr=curr->right;
		}
		prev->right=curr->left;
		curr->left=root->left;
		curr->right=root->right;
		freeNode(root);
		return curr;
	}

	if(strcmp(plate,root->plate)<0) //try to delete the node from the left subtree
	{
		Node temp=delete(root->left,plate);
		root->left=temp;
		return root;
	}
	else //try to delete the node from the right subtree
	{
		Node temp=delete(root->right,plate);
		root->right=temp;
		return root;
	}
}

void freeNode(Node root)
{
	free(root->plate);
	free(root->first);
	free(root->last);
	free(root);
	return;
}

