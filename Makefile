CCOMP = gcc
COMPILEFLAGS = -g

plate: main.o add.o delete.o search.o print.o treeFree.o height.o balance.o header.h
	$(CCOMP) $(COMPILEFLAGS) -o plate treeFree.o print.o main.o add.o delete.o search.o height.o balance.o 
 
main.o: main.c add.c delete.c search.c treeFree.c print.c height.c balance.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c main.c add.c delete.c search.c treeFree.c print.c height.c balance.c 

search.o: search.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c search.c

delete.o: delete.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c delete.c 

add.o: add.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c add.c

treeFree.o: treeFree.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c treeFree.c

print.o: print.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c print.c

height.o: height.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c height.c

balance.o: balance.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c balance.c

make clean: 
	rm main.o print.o treeFree.o search.o delete.o add.o height.o balance.o plate 
