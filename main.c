/* John Stevenson
 * PA4
 * CSE 222
 */

#include "header.h"

/* main handles all user input and top-level operations for the program. 
 * It should initialize and load a buinary search tree and prompt the user for 
 * commands.
 */
int main(int argc,char** argv)
{ 
	char input[256]; //input is used to store commands given by the user
	char plate[32],first[32],last[32]; //plate, first, and last store temporary strings for a license plate, first name, and last name respectively
	int num; //num is used to store a license plate given by the user when deleting 
	Node root=NULL; //initializes binary search tree to be empty

	if(argc!=2) //if the program was not called with a database, print an error message and exit with a return value of 1
	{
		printf("ERROR: improper call of program\n");
		return 1;
	}

	FILE* fp=fopen(argv[1],"r"); //opens the database

	if(fp==0) //if the database does not exist, print an error message and exit the program with a return value of 1
	{
		printf("ERROR: invalid database\n");
		return 1;
	}

	while(fgets(input,256,fp)!=NULL) //loads the database into the binary search tree
	{
		sscanf(input,"%s %s %s",plate,first,last);

		root=add(root,plate,first,last);
	}
	
	printf("Enter a plate or command: "); //initial command message

	while(fgets(input,256,stdin)!=NULL) //loops until the user hits ctrl+d
	{
		input[strlen(input)-1]='\0'; //removes the \n at the end of the user input from fgets
		num=sscanf(input,"*DELETE %s",plate); //parses the input for the delete function
			
		if(strcmp(input,"*DUMP")==0) //prints the contents of the binary search tree in three different ways
		{
			printf("Height: %d\n",height(root));
			if(balance(root))
			{
				printf("Balanced: YES\n");
			}
			else
			{
				printf("Balanced: NO\n");
			}
			printf("LNR Traversal:\n");
			LNR(root);
			printf("\n");
			printf("NLR Traversal:\n");
			NLR(root);
			printf("\n");
			printf("LRN Traversal:\n");
			LRN(root);
			printf("\n");
		}
		else if(num==1) //deletes a plate if it exists
		{
			if(search(root,plate,first,last))
			{
				root=delete(root,plate);
				printf("Success\n");
			}
			else
			{
				printf("PLATE NOT FOUND\n");
			}
		}	
		else //if nothing else has been done, the program will search for a plate
		{
			if(search(root,input,first,last))
			{
				printf("First name: %s\n",first);
				printf("Last name: %s\n",last);
			}
			else
			{
				printf("PLATE NOT FOUND\n");
			}
		}
		printf("Enter a plate or command: "); //prints a prompt for the user 	
	}

	treeFree(root); //frees all memory from the binary search tree created earlier
	printf("\n"); //resets command line
	fclose(fp);
	return 0; //program exits normally with a return value of 0
}

