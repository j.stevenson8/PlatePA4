#include "header.h"

/* Height is a function that finds the height 
 * of a binary search tree. Must be given the 
 * root of a tree. Returns the height of the 
 * tree.
 */

int height(Node root)
{
	if(root==NULL) //if the tree is empty, its height is one
	{
		return -1;
	}

	int lheight=height(root->left);
	int rheight=height(root->right);

	if(lheight>rheight) //if the left subtree is taller than the right one, the height of the tree is the height of the left subtree+1
	{
		return lheight+1;
	}
	else //if the right subtree is taller than the left one, the height of the tree is the height of the right subtree+1
	{
		return rheight+1;
	}
}
