#include "header.h"

/* Balance is a function that determines
 * whether or not a tree is balanced. It
 * does this by comparing the height of 
 * the left subtree and the height of the
 * right subtree. If the absolute value 
 * of their difference is less than or 
 * equal to one for every node in the tree,
 * than the tree is balanced. Must be given
 * the root of a tree. Returns a 1 if the 
 * tree is balanced and a 0 if it is not. 
 */

int balance(Node root)
{
	if(root==NULL)
	{
		return 1;
	}
	
	int lheight=height(root->left); //height of the left subtree
	int rheight=height(root->right); //height of the right subtree
 

	if((balance(root->right))&&(balance(root->left)))
	{
		int num=lheight-rheight;
		if((num<=1)&&(num>=-1))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}
